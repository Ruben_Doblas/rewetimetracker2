import { RouterModule, Routes } from '@angular/router';
import { TrackerComponent } from './components/tracker/tracker.component';
import { AdminComponent } from './components/admin/admin.component';
import { AuthGuardService } from './services/auth-guard.service';
import { HomeComponent } from './components/home/home.component';
import { NewUsersComponent } from './components/new-users/new-users.component';
import { DetailsComponent } from './components/details/details.component';
import { CalendarComponent } from './components/calendar/calendar.component';



const APP_ROUTES: Routes = [

    { 
      path: 'tracker', 
      component: TrackerComponent,
      canActivate: [ AuthGuardService ]
    },

    { 
      path: 'admin', 
      component: AdminComponent,
      canActivate: [ AuthGuardService ]
    },

    { 
      path: 'new-users/:id', 
      component: NewUsersComponent,
      canActivate: [ AuthGuardService ]
    },

    { 
      path: 'details/:id', 
      component: DetailsComponent,
      canActivate: [ AuthGuardService ]
    },
    {
      path: 'calendar', 
      component: CalendarComponent ,
      canActivate: [ AuthGuardService ]
    },
   

    { 
      path: 'home', 
      component: HomeComponent
    },

    { path: '**', redirectTo: 'home', pathMatch: 'full' }

  ];


  export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);
