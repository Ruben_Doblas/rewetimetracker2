import { Injectable } from '@angular/core';
import { User } from '../interfaces/user.interface';
import { AuthService } from './auth.service';
import { UsersService } from './users.service';
import { log } from 'util';

@Injectable({
  providedIn: 'root'
})
export class SyncroService {
userInfoAuth: any;
userId: any; 
private userData: User= {

  id: '',
  name: '',
  email: '',
  idNumber: '',
  role:'',
 

};



  constructor( private userService:UsersService, private auth:AuthService ) { }

  initUser() {
    this.auth.getProfile((err, profile) => {
      this.userInfoAuth = profile;
      console.log("sub "+this.userInfoAuth.sub);
       this.userService.getUser(this.userInfoAuth.sub).subscribe(userI =>{       
        
        if (!userI) {
          console.log("entra");
          
          this.userData.email = this.userInfoAuth.name;
          this.userData.id    = this.userInfoAuth.sub;
          this.userService.newUser( this.userData, this.userData.id  ).subscribe();
        } else {
          console.log("exito:"+this.userData.id );
        }
       }) ;
       
     
    });
    
    
  }
}