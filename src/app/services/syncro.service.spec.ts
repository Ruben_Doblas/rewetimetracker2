import { TestBed } from '@angular/core/testing';

import { SyncroService } from './syncro.service';

describe('SyncroService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SyncroService = TestBed.get(SyncroService);
    expect(service).toBeTruthy();
  });
});
