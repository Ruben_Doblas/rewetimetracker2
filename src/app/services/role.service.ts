import { Injectable } from '@angular/core';
import { User } from '../interfaces/user.interface';
import { UsersService } from '../services/users.service';
import { AuthService } from '../services/auth.service';




@Injectable({
  providedIn: 'root'
})
export class RoleService {

  isAdmin = false;
  adminID ='';
  user:User;

  constructor( 
    private _usersService: UsersService,
    private auth: AuthService
     ) {}
 

  
  getRole( id:any ){
    switch ( id ) {
      case 'auth0|5bc9ebb28f65fb7f29351097':
          this.isAdmin = true;
          break;
      case 'id2':
          this.isAdmin = true;
          break;
      case 'id3':
          this.isAdmin = true;
          break;
      default:
        this.isAdmin = false;

     }
     return this.isAdmin;
 }
}



