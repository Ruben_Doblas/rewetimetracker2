import { Injectable } from '@angular/core';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivate } from '@angular/router';
import { AuthService } from './auth.service';




@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor( private auth: AuthService, private route: Router ) { }

  canActivate( next: ActivatedRouteSnapshot, state: RouterStateSnapshot ){
 
    if ( this.auth.isAuthenticated() ) {

      console.log('Accepted by guard');
      return true;

    } else {

      console.error('Blocked by guard');
      this.route.navigate(['/home']);
      return false;

    }

  }

}
