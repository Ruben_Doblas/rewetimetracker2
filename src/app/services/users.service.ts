import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from '../interfaces/user.interface';
import { AuthService } from 'src/app/services/auth.service';


@Injectable({
  providedIn: 'root'
})
export class UsersService {

  usersURL = 'https://timetracking-f858e.firebaseio.com/users.json';
  userURL = 'https://timetracking-f858e.firebaseio.com/users/';
  userData:any;
  

  constructor( private http: Http, private auth:AuthService ) { }


  newUser( user:User,id:string ){

    let body = JSON.stringify( user );
    let headers = new Headers({
      'Content-Type':'application/json'
    });
    if (id != ""){
      this.userURL = `${ this.userURL }${ id }.json`;
    }
    
    return this.http.put(  this.userURL, body, { headers }  )
          .pipe(map( res=>{
            console.log(res.json());
            return res.json();
          }));
  }

  updateUser( user:User, key$:string ){

    let body = JSON.stringify( user );
    let headers = new Headers({
      'Content-Type':'application/json'
    });

    let url = `${ this.userURL }/${ key$ }.json`;

    return this.http.put(  url , body, { headers }  )
          .pipe(map( res=>{
            console.log(res.json());
            return res.json();
          }));
  }

  getUser( key$:string ){

    let url = `${ this.userURL }/${ key$ }.json`;
    return this.http.get( url )
      .pipe(map( res=>res.json() ));

  }

  /*

  getRole(){
    let userId;
    let role;
    return this.auth.getProfileForRole().subscribe(
       userId =>{
        return this.getUser( userId ).pipe(map( user=> { 
          return role = user.role ;
        }));
       }
     );
    
    
  } */

  getUsers( ){

    return this.http.get( this.usersURL )
      .pipe(map( res=>res.json() ));

  }

  deleteUser( key$:string){

    let url = `${  this.userURL  }/${ key$ }.json`;
    return this.http.delete( url )
        .pipe(map( res => res.json() ));

  }

  




}
