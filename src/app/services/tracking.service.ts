import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Tracking } from '../interfaces/tracking.interface';
import { DayComponent } from '../components/day/day.component';
import { getTime } from 'date-fns'; 
import { UsersService } from './users.service';
import { User } from '../interfaces/user.interface';


@Injectable({
  providedIn: 'root'
})
export class TrackingService {

  trackingURL = 'https://timetracking-f858e.firebaseio.com/tracking.json';
  trackURL = 'https://timetracking-f858e.firebaseio.com/tracking';
  trackingUrlCh;
  returnParam :any;
  currentDay;
  users:any[] = [];
  user: any;
  
  array:any[] = [];
  weekNumber: number;
  totalHoursWeek: any[] = [];
  arrayHours: any[] = [];
  selectUnselect:any;
  allSelected:boolean;
  today: number = Date.now();
  idUserRole = sessionStorage.getItem("idUserRole");
  idUsers: any[] = [];
  
  private tracking : Tracking = {
    start:'',
    finish:'',
    break: 0,
    totalHours:0,
    startTimestamp: null,
    finishTimestamp: null 
      };

  constructor( private http: Http,
               private _usersService: UsersService
    ) {
    // console.log(this.getCalendarWeek(new Date()));
  }

  startDay( id:any  ) {
    let day = new Date();
    this.currentDay = day.getDate() +""+ (day.getMonth()+1) +""+day.getFullYear();
    console.log(this.currentDay);
    let currentWeek = this.getActualCalendarWeek();
    let url = `${ this.trackURL }/${ id }/${currentWeek}/${ this.currentDay }.json`;
    
    return  this.http.get( url ).pipe(map( res=>res.json() )).subscribe( user => {      
      if (user === null){
        this.emptyDate( id, this.currentDay ).subscribe();
        this.updateDay( this.tracking , id ).subscribe();
       }
    else {
      console.log("day already started");
    }
  });
}
  emptyDate(id:any, date: any){
    let body =  {break: '',finish: '',start: ''};
    let headers = new Headers({'Content-Type':'application/json'});
    let currentWeek = this.getActualCalendarWeek();
    this.trackingUrlCh = `${ this.trackURL }/${ id }/${currentWeek}/${ date }.json`;
    return this.http.put(  this.trackingUrlCh, body, { headers }  )
          .pipe(map( res=>{
            console.log(res.json());
            return res.json();
          }));
  }

  newDay( tracking:Tracking ){
    
    let body = JSON.stringify( tracking );
    let headers = new Headers({
      'Content-Type':'application/json'
    });

    return this.http.post(  this.trackingURL, body, { headers }  )
          .pipe(map( res=>{
            console.log(res.json());
            return res.json();
          }));
  }


  updateDay( tracking:Tracking, id:string ){
    let day = new Date();
    this.currentDay = day.getDate() +""+ (day.getMonth()+1) +""+day.getFullYear();
    let body = JSON.stringify( tracking );
    let headers = new Headers({
      'Content-Type':'application/json'
    });
    console.log(this.currentDay);
    let currentWeek = this.getActualCalendarWeek();

    let url = `${ this.trackURL }/${ id }/${ currentWeek }/${ this.currentDay }.json`;
    
    if (tracking.finish == "") {
      tracking.totalHours = 0;
    }
    console.log(url);
    console.log(body);
    return this.http.put(url, body, { headers }  ).pipe(map( res=>{
            console.log(res.json());
            return res.json();
          }));
  }
  editDay(tracking:Tracking, id:string) {
    let ts = new Date(tracking.startTimestamp);
    let day = ts.getDate()+""+ (ts.getMonth()+1) +""+ts.getFullYear();
    let week = this.getCalendarWeek(ts);
    let headers = new Headers({
      'Content-Type':'application/json'
    });
    let startHoursArr = tracking.start.split(":");
    let finishHoursArr = tracking.finish.split(":");
    tracking.startTimestamp = new Date(ts.getFullYear(),(ts.getMonth()),ts.getDate(),
    // tslint:disable:radix
    parseInt(startHoursArr[0]),parseInt(startHoursArr[1]),0);
     
    tracking.finishTimestamp = new Date(ts.getFullYear(),(ts.getMonth()),ts.getDate(),
    parseInt(finishHoursArr[0]),parseInt(finishHoursArr[1]),0);
    
    tracking.totalHours = getTime(tracking.finishTimestamp) - getTime(tracking.startTimestamp);
    // console.log("Descanso: " + this.tracking.break);
    tracking.totalHours = (tracking.totalHours/(3600000)) - (tracking.break/60);
    // console.log("Tiempo total" + this.totalTime/(3600*1000)); 
    tracking.totalHours = Math.round(tracking.totalHours * 100) /100;
    
    console.log("Tracking" + tracking);
    
    if (tracking.finish == "") {
      tracking.totalHours = 0;
    }
    let body = JSON.stringify( tracking );
    let url = `${ this.trackURL }/${ id }/${ week }/${ day }.json`;
    return this.http.put(url, body, { headers }  ).pipe(map( res=>{
      console.log("from editDay: " + res.json());
      return res.json();
    }));

  }

  getDay( id:string, day:string, ){
    let currentWeek = this.getActualCalendarWeek();
    let url = `${ this.trackURL }/${ id }/${ currentWeek }/${ day }.json`;
    return this.http.get( url )
      .pipe(map( res=>res.json() ));

  }
  getDays(  id:string, dayWeek: number ){

    let url = `${ this.trackURL }/${ id }/${ dayWeek }.json`;
    return this.http.get( url )
      .pipe(map( res=>res.json() ));

  }


  deleteDay( key$:string){

    let url = `${  this.trackURL  }/${ key$ }.json`;
    return this.http.delete( url )
        .pipe(map( res => res.json() ));

  }

  getActualCalendarWeek(){
    let date = new Date();
    let d:any = new Date(Date.UTC(date.getFullYear(), date.getMonth(), date.getDate()));
    let dayNum = d.getUTCDay() || 7;
    d.setUTCDate(d.getUTCDate() + 4 - dayNum);
    let yearStart:any = new Date(Date.UTC(d.getUTCFullYear(),0,1));
    return Math.ceil((((d - yearStart) / 86400000) + 1)/7);
  }
  getCalendarWeek( fecha : any ){
    let tdt:any = new Date(fecha.valueOf());
    let dayn = (fecha.getDay() + 6) % 7;
    tdt.setDate(tdt.getDate() - dayn + 3);
    let firstThursday:any = tdt.valueOf();
    tdt.setMonth(0, 1);
    if (tdt.getDay() !== 4) 
      {
     tdt.setMonth(0, 1 + ((4 - tdt.getDay()) + 7) % 7);
       }
    return 1 + Math.ceil((firstThursday - tdt) / 604800000);
  }

  getWeeksOfMonth(year: any, month:any) {
    let datest = new Date().getMonth();
    let lastDay = new Date(year, month + 1 , 0).getDate() ;
    let firstWeek:any = this. getCalendarWeek(new Date(year, month ,1));
    let lastWeek:any = this. getCalendarWeek(new Date(year, month ,lastDay));
    let totalWeeksNumber = lastWeek - firstWeek;
    let arrWeeks = [];
    
    for(let i = firstWeek;i <= lastWeek; i++){
        arrWeeks.push(i);
      }

    return arrWeeks;
  }

  getActualMonth(idUsersMonth:User){
    idUsersMonth.daysMonth = [];

    // console.log(idUsersMonth);
    
    let weekMonth = this.getWeeksOfMonth( new Date().getFullYear(), new Date().getMonth()-1);
    // console.log(weekMonth);
    
    
    // console.log(idUsersMonth);
    

      console.log(idUsersMonth);
        
        console.log(weekMonth.length);
        for (let i = 0; i <= weekMonth.length; i++) {
          
          // console.log(idUsersMonth);
          this.getDays(idUsersMonth.id , weekMonth[i])
          .subscribe((dia:Tracking) =>{

            Object.keys(dia).forEach(elemento => {

              idUsersMonth.daysMonth.push(dia[elemento]);
              
            });
 
          });  
        }
        
  return idUsersMonth;
  
  
  
}



  




}
