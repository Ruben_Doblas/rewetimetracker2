export interface Tracking {
    start:string;
    finish:string;
    break:number;
    totalHours:number;
    startTimestamp:Date;
    finishTimestamp:Date;
}
