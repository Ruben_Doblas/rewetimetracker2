
export interface User{
    daysMonth?: any[];
    id: any;
    name: string;
    email: string;
    idNumber: string;
    role: string;
    key$?: string;
    hoursWeek?:any[];
}
