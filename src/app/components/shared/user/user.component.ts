import { Component, OnInit, AfterContentInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { User } from '../../../interfaces/user.interface';
import { UsersService } from '../../../services/users.service';
import { AuthService } from '../../../services/auth.service';


@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  profile: any = {};
  id:string;
  users:any[] = [];
  loading = true;
  admin = false;
  myId: any;

  public user: User = {

    id: '',
    name: '',
    email: '',
    idNumber: '',
    role: ''

  };

  constructor( 
    private _usersService: UsersService,
    private router:Router,
    private route:ActivatedRoute,
    public auth: AuthService
    ) { 

      auth.handleAuthentication();

      this._usersService.getUsers()
        .subscribe( data =>{
          this.users = data;
          this.loading = false;
        });
        if (this.auth.userProfile) {
          this.profile = this.auth.userProfile;    
        } else {
          this.auth.getProfile((err, profile) => {
            this.profile = profile;
            // console.log(profile);
          });
        }
      
        

    }

    

    ngOnInit() {

      if (this.auth.userProfile) {
        this.profile = this.auth.userProfile;
            
      } else {
        this.auth.getProfile((err, profile) => {
          this.profile = profile;
          
          // console.log(profile);
        });
      }
      
      
    }

}
