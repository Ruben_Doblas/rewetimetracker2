import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../services/auth.service';
import { UsersService } from '../../../services/users.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  profile: any = {};
  users:any[] = [];
  loading = true;
  myId: any;
  admin = false;
  idUserRole = sessionStorage.getItem("idUserRole");
  isUserLoggedIn: boolean;

  constructor( public auth: AuthService, private _usersService:UsersService) {



     // If Admin Rewe Test
 
     if( this.idUserRole === 'adminId1' ){

      this.admin = true;
    
    } // If Chris
    
    else if( this.idUserRole === 'auth0|5bd700a08e30fa598c0beb53' ) {

      this.admin = true;

    } // If Sebastian
    
    else if( this.idUserRole === 'adminSebastian' ) {

      this.admin = true;

    }

    auth.handleAuthentication();
    this._usersService.getUsers()
        .subscribe( data =>{
          this.users = data;
          this.loading = false;
        });
        
        

        
    
    }

  ngOnInit() {

    if (this.auth.userProfile) {
      this.profile = this.auth.userProfile;    

    } else  {
      this.auth.getProfile((err, profile) => {
        this.profile = profile;

        // console.log(this.auth);
      });
    }
    
    
  }

  

}
