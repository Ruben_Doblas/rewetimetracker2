import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { User } from '../../interfaces/user.interface';
import { UsersService } from '../../services/users.service';
import { AuthService } from '../../services/auth.service';
import { Location} from '@angular/common';



@Component({
  selector: 'app-new-users',
  templateUrl: './new-users.component.html',
  styleUrls: ['./new-users.component.css']
})
export class NewUsersComponent implements OnInit {


  profile: any;

  public user: User = {

    id: '',
    name: '',
    email: '',
    idNumber: '',
    role: ''
   

  };

  new = false;
  id:string;
  okNewUser = false;
  okEditUser = false;
  error = false;
  isAdmin:any;
  // isAdmin: boolean;

  constructor( 
    private _usersService: UsersService,
    private router:Router,
    private route:ActivatedRoute,
    private auth: AuthService,
    private _location: Location
    ) { 

      this.route.params
        .subscribe( parameters=>{

          this.id = parameters['id'];
          if( this.id !== 'new' ){

            this._usersService.getUser( this.id )
                  .subscribe( user => {
                    this.user = user;                                   
                  } );
          

          }
        });



        
        

        

    }

    backClicked() {
      this._location.back();
  }

    save(){
      console.log(this.user);

    if( this.id ==='new' ){
      // Inserting
      this._usersService.newUser( this.user , "" )
            .subscribe( data=>{
                  this.router.navigate(['/new-users',data.name]);
                  this.okNewUser = true;
            },
            error=> console.error(error));
            // this.error = true;
    } else{
      // Updating
      this._usersService.updateUser( this.user, this.id )
            .subscribe( data=>{
                  console.log(data);
                  this.okEditUser = true;
            },
            error=> console.error(error));
            // this.error = true;
    }

  }

  addNew( forma:NgForm ){

    this.router.navigate(['/new-users','new']);

  }

  ngOnInit() {

    if (this.auth.userProfile) {
      this.profile = this.auth.userProfile;    
    } else {
      this.auth.getProfile((err, profile) => {
        this.profile = profile;
        console.log(profile);
      });
    }
    
    
  }
  
    }
  
  
  



