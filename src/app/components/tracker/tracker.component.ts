import { Component } from '@angular/core';
import { Tracking } from '../../interfaces/tracking.interface';
import { TrackingService } from '../../services/tracking.service';
import { NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { User } from '../../interfaces/user.interface';
import { UsersService } from '../../services/users.service';
import { SyncroService } from 'src/app/services/syncro.service';
import { AuthService } from 'src/app/services/auth.service';
import { getTime } from 'date-fns'; 
import { log } from 'util';



@Component({
  selector: 'app-tracker',
  templateUrl: './tracker.component.html',
  styleUrls: ['./tracker.component.css']
})
export class TrackerComponent {

  time : Date;
  timeStart;
  timeEnd;
  id;
  day;
  break = 0;
  userId:any;
  diaPrueba = new Date('');
  started: boolean;
  ended: boolean;
  totalTime;
  breakFilled: boolean;
  private tracking : Tracking = {
      start:'',
      finish:'',
      break: 0,
      totalHours:0,
      startTimestamp : this.diaPrueba ,
      finishTimestamp: this.diaPrueba
        };
  private user: User = {
    id: '',
    name: '',
    email: '',
    idNumber: '',
    role:'',
    
  };

  data: Tracking[] = [];
  
  
  constructor( private _trackingService : TrackingService,
               private _usersService: UsersService,
               private router:Router,
               private route:ActivatedRoute,
               private auth:AuthService,
               private _syncroService: SyncroService){

                let day = new Date();
                let date = day.getDate() +""+ (day.getMonth()+1) +""+day.getFullYear();

                this.auth.getProfile((err, profile) => {
                  this.userId = profile.sub;
                  console.log("profile" + profile.sub);

                  // Set in session the User ID/SUB
                  
                  if (typeof(Storage) !== "undefined") {
                    // Store
                    sessionStorage.setItem("idUserRole", profile.sub);
                    
                  }

                  // If Admin Rewe Test
 
                  if( profile.sub === 'adminId1' ){

                    this.router.navigate(['/admin']);
                  
                  } // If Chris
                  
                  else if( profile.sub === 'auth0|5bd700a08e30fa598c0beb53' ) {

                    this.router.navigate(['/admin']);

                  } // If Sebastian
                  
                  else if( profile.sub === 'adminSebastian' ) {

                    this.router.navigate(['/admin']);

                  }



                  // End of redirect in case is not an Admin 

                  this._usersService.getUser( this.userId )
                          .subscribe( user => {
                            if(user){
                              this.user = user;
                            } else {
                             this._syncroService.initUser( );
                            }
                          });
                  console.log("userid " + this.userId);
                  this._trackingService.getDay( this.userId, date )
                              .subscribe( tracking => {
                            if (tracking) {
                              this.tracking = tracking;
                            } 
                            else {
                              this._trackingService.startDay( this.userId );
                            }
                            console.log("user: "+ JSON.stringify(this.user));
                            console.log("tracking: "+ JSON.stringify(this.tracking));
                            console.log("Start timestamp" + this.tracking.startTimestamp);
                            
                            if (this.tracking.start === "") {
                              this.started = false;
                            } else {
                              this.started = true;
                              this.timeStart =  this.tracking.start;
                              

                            }

                            if (this.tracking.finish === "") {
                              this.ended = false;
                            } else {
                              this.ended = true;
                              this.timeEnd = this.tracking.finish;
                              
                            }
                            if( this.tracking.break){
                              this.break = this.tracking.break;
                              this.breakFilled = true;
                            }
                            
                            
                            this.totalTime = getTime(this.tracking.finishTimestamp) - getTime(this.tracking.startTimestamp);
                            
                            this.totalTime = ((this.totalTime/(3600*1000)) - (this.tracking.break/60)).toFixed(2);
                            
                          });
                });                                
     }

              
    

  onStart(){
    this.time = new Date();
    console.log(('0'+this.time.getHours()).slice(-2));
    this.timeStart = ('0'+this.time.getHours()).slice(-2) + ':' + ('0'+this.time.getMinutes()).slice(-2);
    this.tracking.startTimestamp = this.time;
    this.tracking.start = this.timeStart;
    this._trackingService.updateDay( this.tracking , this.userId ).subscribe();
    this.started = !this.started;

  }
  onEnd(){
    this.time = new Date();
    console.log(this.time);
    this.timeEnd = ('0'+this.time.getHours()).slice(-2) +':' + ('0'+this.time.getMinutes()).slice(-2);
    this.tracking.finishTimestamp = this.time;
    this.tracking.finish = this.timeEnd;
   
    this.ended = !this.ended;
     
    this.totalTime = getTime(this.tracking.finishTimestamp) - getTime(this.tracking.startTimestamp);
    // console.log("Descanso: " + this.tracking.break);
    this.totalTime = ((this.totalTime/(3600*1000)) - (this.tracking.break/60)).toFixed(2);
    // console.log("Tiempo total" + this.totalTime/(3600*1000)); 

    this.tracking.totalHours = this.totalTime;
    this._trackingService.updateDay( this.tracking , this.userId ).subscribe();
  }
  onMinus(){
    if (this.break != this.tracking.break) {
      this.break = this.break > 0 ? this.break - 5 : this.break ;
    }
    
  }
  onPlus(){
    this.break = (this.break + 5);
  }
  onSetBreak(value: number) {
    if (value >= 0) { 
      this.tracking.break = value;
      
      console.log("break: " + JSON.stringify(this.tracking));
      this._trackingService.updateDay( this.tracking , this.userId ).subscribe();
      this.breakFilled = true;
    }
  }

  onKeyUp(value: any) {
    console.log(value);
    if (isNaN(value)) {
      this.break = 0;
    } else {
      // tslint:disable-next-line:radix
      this.break = parseInt(value);
    }
  }
  

}
