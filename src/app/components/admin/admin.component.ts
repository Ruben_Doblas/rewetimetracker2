import { Component} from '@angular/core';
import { UsersService } from '../../services/users.service';
import { TrackingService } from '../../services/tracking.service';
import { Tracking } from 'src/app/interfaces/tracking.interface';
import { Router } from '@angular/router';
import { ExcelService } from '../../services/excel.service';
import { log } from 'util';
import { startOfISOWeek } from 'date-fns';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent {

  users:any[] = [];
  dataMonth:any[] = [];
  user: any;
  
  role: any;
  arrayUsers:any[] = [];
  weekNumber: number;
  totalHoursWeek: any[] = [];
  arrayHours: any[] = [];
  selectUnselect:any;
  allSelected:boolean;
  today: number = Date.now();
  idUserRole = sessionStorage.getItem("idUserRole");
  idUsers: any[] = [];
  idUsersMonth:any[] =[];

  
  constructor(private _usersService: UsersService, 
              private _trackingService: TrackingService,
              private _excelService: ExcelService) { 
                
 

    this._usersService.getUsers().subscribe( 
      dataUsers =>{
        this.users = dataUsers;
  
        
        Object.keys(dataUsers).forEach(key => {        
        this.arrayUsers.push(dataUsers[key]);
        });
        console.log(this.arrayUsers);
        this.weekNumber = this._trackingService.getActualCalendarWeek();
      
        for (let index = 0; index < this.arrayUsers.length; index++) {
        
          this.user = Object.keys(this.users);
          this.users[this.user[index]].hoursWeek = [];        
          this._trackingService.getActualMonth(this.users[this.user[index]]);
          let key = this.arrayUsers[index].id;
          this._trackingService.getDays(key, this.weekNumber).subscribe((day:Tracking) =>{
              this.totalHoursWeek[index] = 0;
              Object.keys(day).forEach(elemento => {
                this.users[this.user[index]].hoursWeek.push(day[elemento]);               
                this.totalHoursWeek[index] += parseFloat(day[elemento].totalHours);
              });
              if (isNaN(this.totalHoursWeek[index])) {
                this.totalHoursWeek[index] = 0; 
              }
              this.users[this.user[index]].totalHours = this.totalHoursWeek[index];
            });            
          }
        });
          

          // console.log(this.users[1]);
    

  }


  onSelectAll(){

    let all = false;
    this.allSelected = true;
    Object.keys(this.users).forEach(i => {
      if (!this.users[i].selected) {
        this.allSelected = false;
      } 
     
    });

    if (this.allSelected) {
    Object.keys(this.users).forEach(i => {
      
        this.users[i].selected = false;        
        all = false;
      });
    } else {
      this.idUsers = [];
      Object.keys(this.users).forEach(i => {
        this.users[i].selected = true;
        this.idUsers.push(this.users[i]);
        all = true;
      });
    }
    if (!all) {
      this.idUsers = [];
    }
    

  }

  onCheckboxClick(id:any){
    let unico = true;
      for (let i = 0; i < this.user.length; i++) {
        
        if(this.idUsers[i] == id){
          this.idUsers.splice(i, 1);
          unico = false;
        } 
      }
      if(unico) {
        this.idUsers.push(id);      
      } 
    
  }


  deleteUser( key$:string){

    const r = confirm( 'Do you really want to delete this user?' );
    
    if (r === true) {

      this._usersService.deleteUser(key$)
      .subscribe( response=>{
        if( response ){
          console.error(response);
        }else{
          // All OK
          delete this.users[key$];
        }
      });

    } 

  }

  
  ExcelMonth(year: any, month:any){
    let lastDay = new Date(year, month + 1 , 0).getDate() ;
    let firstWeek:any = this._trackingService.getCalendarWeek(new Date(year, month ,1));
    let lastWeek:any = this._trackingService.getCalendarWeek(new Date(year, month ,lastDay));
 
    let weekMonth = this._trackingService.getWeeksOfMonth( new Date().getFullYear(), new Date().getMonth());
    console.log(weekMonth);

  }
  ActualMonth(){
  // console.log(this.idUsersMonth);
  console.log(this.idUsers);
  // this._trackingService.downloadExcelActualMonth(this.idUsers);
  for (let index = 0; index <= this.idUsers.length; index++) {
        
    this._excelService.exportAsExcelFile(this.idUsers[index].daysMonth, this.idUsers[index].name);
    console.log(this.idUsers[index].daysMonth);
    
    console.log(this.idUsers[index]);
    
    }

  }
  downloadExcel(){
  console.log("semana: "+ JSON.stringify(this.idUsers));
  for (let index = 0; index <= this.idUsers.length; index++) {
    this._excelService.exportAsExcelFile(this.idUsers[index].hoursWeek, this.idUsers[index].name);
  
    
  }
    
  }

}
