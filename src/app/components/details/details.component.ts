import { Component  } from '@angular/core';
import { UsersService } from '../../services/users.service';
import { Router, ActivatedRoute } from '@angular/router';
import { User } from '../../interfaces/user.interface';
import { Tracking } from '../../interfaces/tracking.interface';
import { TrackingService } from '../../services/tracking.service';
import { ExcelService } from '../../services/excel.service';


@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent{

  today: number = Date.now();
   user: User = {

    id: '',
    name: '',
    email: '',
    idNumber:'',
    role:''

  };
  tracks: any[] = [0,0,0,0,0];
  daysName: any[] = ['Monday', 'Tuesday', 'Wendesday','Thursday', 'Friday'];
  totalHoursWeek: any = 0;
  weekNumber: number;
  id:string;
  editable=false;
  selectedId;
  totalHoursOnEdit: number;

  constructor(
    private _usersService:UsersService,
    private _router:Router,
    private _route:ActivatedRoute,
    private _trackingService: TrackingService,
    private _excelService: ExcelService
    ) {  this.refreshData(); }


  refreshData(){
    this._route.params.subscribe( parameters=>{

      this.id = parameters['id'];
      this._usersService.getUser( this.id ).subscribe( user => this.user = user );
      this.weekNumber = this._trackingService.getActualCalendarWeek();
      this._trackingService.getDays( this.id, this.weekNumber).subscribe( (day: Tracking) => {
        // console.log(day);
        Object.keys(day).forEach(element => {
          if (!isNaN(this.totalHoursWeek)) {
            this.totalHoursWeek = parseFloat(this.totalHoursWeek) + parseFloat(day[element].totalHours);
            this.totalHoursWeek = Math.round(this.totalHoursWeek*100)/100;
          }
          let dia = (new Date(day[element].startTimestamp).getDay() -1);
          // console.log(dia + " - " + JSON.stringify(day[element]));
          
          this.tracks.splice(dia, 1, day[element]);
          // console.log(this.tracks);
          // console.log(this.editable);
        });

      this.progressHours = (this.totalHoursWeek / 40) * 100;
      // console.log(this.tracks);
      // console.log(this.daysName);

      });

      if(!isNaN( this.totalHoursWeek )){

        // Is not a number
        this.notANumber = true;

      } else {

        // Is a number
        this.notANumber = false;
      }
    });
  }


  exportAsXLSX(): void {
    this._excelService.exportAsExcelFile(this.tracks, 'TrackExcel');
  }

  onEdit(id:any){
    // console.log("ID: " + id);
    // console.log("Day: "+ JSON.stringify(this.tracks[id]));
    this.editable=true;
    // console.log(this.editable);
    this.selectedId = id;

  }

  onSave(id:any) {
    let userId = this.user.id;
    this._trackingService.editDay(this.tracks[id], userId).subscribe();
    this.totalHoursWeek = this.totalHoursOnEdit + this.tracks[id].totalHours;
    if (isNaN(this.totalHoursWeek)) {
      this.totalHoursWeek = 0;
    }
    this.selectedId = null;
  }
}
