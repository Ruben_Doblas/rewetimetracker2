import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { AdminComponent } from './components/admin/admin.component';
import { TrackerComponent } from './components/tracker/tracker.component';
import { CalendarComponent } from './components/calendar/calendar.component';
import { DayComponent } from './components/day/day.component';
import { HomeComponent } from './components/home/home.component';
import { NewUsersComponent } from './components/new-users/new-users.component';
import { UserComponent } from './components/shared/user/user.component';
import { DetailsComponent } from './components/details/details.component';
// Routes
import { APP_ROUTING } from './app.routes';
// Pipes
import { CleanImagesPipe } from './pipes/clean-images.pipe';
import { KeysPipe } from './pipes/keys.pipe';
// Services
import { AuthService } from './services/auth.service';
import { AuthGuardService } from './services/auth-guard.service';
import { UsersService } from './services/users.service';
import { ExcelService } from './services/excel.service';
import { TrackingService } from './services/tracking.service';
import { RoleService } from './services/role.service';

// Calendar
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FlatpickrModule } from 'angularx-flatpickr';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { NgbModule, NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { SyncroService } from './services/syncro.service';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    AdminComponent,
    TrackerComponent,
    CalendarComponent,
    CleanImagesPipe,
    DayComponent,
    HomeComponent,
    NewUsersComponent,
    KeysPipe,
    DetailsComponent,
    UserComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpModule,
    APP_ROUTING,
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    }),
    NgbModule,
    NgbModalModule,
    FlatpickrModule.forRoot(),

    
  ],
  providers: [
    AuthService,
    AuthGuardService,
    UsersService,
    TrackingService,
    ExcelService,
    RoleService,
    SyncroService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
